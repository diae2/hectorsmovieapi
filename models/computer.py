from config.database import Base
from sqlalchemy import Column, Integer, String

class Computer(Base):
    __tablename__ = "computers"
    
    id = Column(Integer, primary_key = True, autoincrement="auto")
    brand = Column(String)
    model = Column(String)
    color = Column(String)
    ram = Column(String)
    storage = Column(String)