from pydantic import BaseModel, Field

class Computer(BaseModel):
    brand: str = Field(min_length=1, max_length=30)
    model: str = Field(min_length=1, max_length=30)
    color: str = Field(min_length=1, max_length=30)
    ram: str = Field(min_length=1, max_length=30)
    storage: str = Field(min_length=1, max_length=30)
    
    class Config:
        json_schema_extra = {
            "example": {
                "brand": "Apple",
                "model": "Pro M3",
                "color": "Black",
                "ram": "16 GB",
                "storage": "256 GB",
            }
        }