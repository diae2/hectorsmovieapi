from models.computer import Computer as ComputerModel
from schemas import computer
from fastapi.encoders import jsonable_encoder

class ComputerService:
    def __init__(self, db) -> None:
        self.db = db

    def get_computers(self):
        result = self.db.query(ComputerModel).all()
        return jsonable_encoder(result), 200
    
    def get_computer_by_id(self, id: int):
        result = self.db.query(ComputerModel).filter(ComputerModel.id == id).first()
        if not result:
            return {"message": "Not Found"}, 404
        return jsonable_encoder(result), 200
    
    def get_computers_by_brand(self, brand: str):
        result = self.db.query(ComputerModel).filter(ComputerModel.brand == brand).all()
        if not result:
            return {"message": "Not Found"}, 404
        return jsonable_encoder(result), 200
    
    def create_computer(self, computer: computer.Computer):
        new_computer = ComputerModel(**computer.model_dump())
        self.db.add(new_computer)
        self.db.commit()
        self.db.refresh(new_computer)
        return jsonable_encoder(new_computer), 200
    
    def update_computer(self, id: int, computer: computer.Computer):
        result = self.db.query(ComputerModel).filter(ComputerModel.id == id).first()
        if not result:
            return {"message": "Not Found"}, 404
        for key, value in computer.dict().items():
            setattr(result, key, value)
        self.db.commit()
        self.db.refresh(result)
        return jsonable_encoder(result), 200
    
    def delete_computer(self, id: int):
        result = self.db.query(ComputerModel).filter(ComputerModel.id == id).first()
        if not result:
            return {"message": "Not Found"}, 404
        self.db.delete(result)
        self.db.commit()
        return {"message": "Computer Was Successfully Deleted"}, 200