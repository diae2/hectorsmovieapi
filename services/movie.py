from fastapi.encoders import jsonable_encoder
from models.movie import Movie as MovieModel
from schemas.movie import Movie

class MovieService():
    def __init__(self, db) -> None:
        self.db = db
        
    def get_movies(self):
        result = self.db.query(MovieModel).all()
        if not result:
           return {"message": "No Movies Saved"}, 404 
        return jsonable_encoder(result), 200
     
    def get_movie_by_id(self, id):
        result = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        if not result:
            return {"message": "Not Found"}, 404
        return jsonable_encoder(result), 200
    
    def get_movies_by_category(self, category):
        result = self.db.query(MovieModel).filter(MovieModel.category == category).all()
        if not result:
            return {"message": "Not Found"}, 404
        return jsonable_encoder(result), 200
    
    def create_movie(self, movie: Movie):
        new_movie = MovieModel(**movie.model_dump())
        self.db.add(new_movie)
        self.db.commit()
        return {"message": "Movie Succesfully Created"}, 200
        
    def delete_movie(self, id):
        result = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        if not result:
            return {"message": "Not Found"}, 404
        self.db.delete(result)
        self.db.commit()
        return {"message": "Movie Succesfully Deleted"}, 200
    
    def update_movie(self, id, movie: Movie):
        result = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        if not result:
            return {"message": "Not Found"}, 404

        result.title = movie.title
        result.overview = movie.overview
        result.year = movie.year
        result.rating = movie.rating
        result.category = movie.category
        self.db.commit()
        return {"message": "Movie Succesfully Updated"}, 200