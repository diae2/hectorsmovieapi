from fastapi import APIRouter, Path, Query, Body
from fastapi.responses import JSONResponse
from config.database import Session
from schemas.computer import Computer
from services.computer import ComputerService

computer_router = APIRouter()

# Get Computers
@computer_router.get('/computers', tags=['computers'], response_model=list[Computer], status_code=200)
def getComputers() -> JSONResponse:
    db = Session()
    result, status = ComputerService(db).get_computers()
    return JSONResponse(content=result, status_code=status)

# Get Computers By ID
@computer_router.get('/computers/{id}', tags=['computers'], response_model=Computer, status_code=200)
def getComputersByID(id: int = Path(ge=1, le=2000)) -> JSONResponse:
    db = Session()
    result, status = ComputerService(db).get_computer_by_id(id)
    return JSONResponse(content=result, status_code=status)

# Get Computers By Brand
@computer_router.get('/computers/', tags=['computers'], response_model=list[Computer], status_code=200)
def getComputersByBrand(brand: str = Query(min_length=1, max_length=30)) -> JSONResponse:
    db = Session()
    result, status = ComputerService(db).get_computers_by_brand(brand)
    return JSONResponse(content=result, status_code=status)

# Create Computer
@computer_router.post('/computers', tags=['computers'], response_model=Computer, status_code=200)
def createComputer(computer: Computer) -> JSONResponse:
    db = Session()
    result, status = ComputerService(db).create_computer(computer)
    return JSONResponse(content=result, status_code=status)

# Delete Computer 
@computer_router.delete('/computers/{id}', tags=['computers'], response_model=dict, status_code=200)
def deleteComputer(id: int = Path(ge=1, le=2000)) -> JSONResponse:
    db = Session()
    result, status = ComputerService(db).delete_computer(id)
    return JSONResponse(content=result, status_code=status)

# Update Computer    
@computer_router.put('/computers/{id}', tags=['computers'], response_model=Computer, status_code=200)
def updateComputer(id: int = Path(ge=1, le=2000), computer: Computer = Body()) -> JSONResponse:
    db = Session()
    result, status = ComputerService(db).update_computer(id, computer)
    return JSONResponse(content=result, status_code=status)
