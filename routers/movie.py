from fastapi import Path, Query, APIRouter
from fastapi.responses import JSONResponse
from config.database import Session
from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()
        
@movie_router.get('/movies', tags=['movies'], response_model=Movie, status_code=200)
def getMovies() -> JSONResponse:
    db = Session()
    result, status = MovieService(db).get_movies()
    return JSONResponse(content=result, status_code=status)

@movie_router.get('/movies/{id}', tags=['movies'], response_model=Movie, status_code=200)
def getMovieById(id: int = Path(ge = 1, le = 2000)) -> JSONResponse:
    db = Session()
    result, status = MovieService(db).get_movie_by_id(id)
    return JSONResponse(content=result, status_code=status)

@movie_router.get('/movies/', tags=['movies'], response_model=Movie, status_code=200)
def getMoviesByCategory(category: str = Query(min_length=1, max_length=15)) -> JSONResponse:
    db = Session()
    result, status = MovieService(db).get_movies_by_category(category)
    return JSONResponse(content=result, status_code=status)

@movie_router.post('/movies/', tags=['movies'], response_model=Movie, status_code=200)
def createMovie(movie: Movie) -> JSONResponse:
    db = Session()
    result, status = MovieService(db).create_movie(movie)
    return JSONResponse(content=result, status_code=status)
    
@movie_router.delete('/movies/{id}', tags=['movies'], response_model=Movie, status_code=200)
def deleteMovie(id: int) -> JSONResponse:
    db = Session()
    result, status = MovieService(db).delete_movie(id)
    return JSONResponse(content=result, status_code=status)

@movie_router.put('/movies/{id}', tags=['movies'], response_model=Movie, status_code=200)
def updateMovie(id: int, movie: Movie) -> JSONResponse:
    db = Session()
    result, status = MovieService(db).update_movie(id, movie)
    return JSONResponse(content=result, status_code=status)